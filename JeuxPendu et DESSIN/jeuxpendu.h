#ifndef DEF_JEUXPENDU
#define DEF_JEUXPENDU

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void chercherchar(char chain[], char tr, int positionValide[]);
char saisie();
int comptchar(char chain[]);
void printTabNumber(int tab[],int nbCase);
void etoile(char motmystere[],char chainetoile[]);
void copietableau(char motMystere[],char chainEtoile[],int placement[]);
int nbessai(int placement[]);
void selectJoueur(char motMystere[]);
void anti_triche();
void toutEnMAJ(char chain[]);
int compteMot(FILE* fichier);
void motAleatoir(char motMystere[]);


#endif // DEF_JEUXPENDU


