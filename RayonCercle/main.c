#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    float Pi=3.14159,surface;
    int rayon;
    printf("Quelle est le rayon ?\n");
    scanf("%d",&rayon);
    surface= Pi*pow(rayon,2);
    printf("La surface du cercle de Rayon %d est %f\n\n",rayon,surface);

    return 0;
}
