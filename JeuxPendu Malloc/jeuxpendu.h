#ifndef DEF_JEUXPENDU
#define DEF_JEUXPENDU
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


void chercherchar(char chain[], char tr, int positionValide[], int *nbr);
char saisie();
int comptchar(char chain[]);
void printTabNumber(int tab[],int nbCase);
void etoile(char tabetoile[],int nbChar);
void copietableau(char motMystere[],char chainEtoile[],int placement[]);
int nbessai(int bonnelettre,int essai);

#endif // DEF_JEUXPENDU


