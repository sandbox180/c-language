#include <stdio.h>
#include <stdlib.h>

double Moyennetableau(int tableau[],int taille)
{
    int i=0,somme=0;
    double moyenne;
    for(i=0; i<taille; i++)
    {
        somme=somme + tableau[i];
    }
    moyenne = somme/taille;
    return moyenne;
}


int main()
{
 int i;
 int tableau[5]={2,4,6,8,10};
 printf("La moyenne des case du Tableau est %f\n",Moyennetableau(tableau,5));
 return 0;
}

