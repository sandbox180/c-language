#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LIGNE 12
#define COLONE 12
#define TAILLLE_MAX COLONE * LIGNE +1

void copieravant();
void copierfin();
void copiercoller();
int choixniveau=6;

int main()
{
    FILE *temp = NULL, *fichier=NULL;

    int lignefichier[TAILLLE_MAX];
    int i=0;
    for(i=0; i<TAILLLE_MAX; i++)
        lignefichier[i]=0;

    temp = fopen("temp.lvl","w");
    fichier = fopen("niveau.lvl","r");

    copieravant(); // etape 1

    fseek(temp,0,SEEK_END); // etape 2

    for(i=0; i<TAILLLE_MAX;i++)
            fprintf(temp,"%d",lignefichier[i]);

    fclose(fichier);
    fclose(temp);

    copierfin(); // etape 3

    copiercoller(); //Etape 4

    return 0;
}

  void copieravant()
{
    FILE *fichier=NULL, *temp=NULL;
    int *lireChar=NULL;
    int i=0,t=0;
    char lignefichier[TAILLLE_MAX];

    fichier = fopen("niveau.lvl","r");
    temp = fopen("temp.lvl","w+");

    if(fichier == NULL)
        { printf("Copier Avant n a pas marcher !"); exit(0); }


    for(i=0; i<choixniveau-1; i++)
    {
         fgets(lignefichier,TAILLLE_MAX,fichier); // On lit le caract�re
         fprintf(temp,lignefichier);
    }
    printf("%s",lignefichier);

    fclose(fichier);
    fclose(temp);
}

void copierfin()
{
    FILE *fichier=NULL, *temp=NULL, *test;
    int i=0,t=0;
    char lignefichier[TAILLLE_MAX];
    char caractereActuel;

    temp = fopen("temp.lvl","a");
    fichier = fopen("niveau.lvl","r");

    if(fichier == NULL)
        { printf("Copier FIN n a pas marcher !"); exit(0); }

    for(i=0; i<choixniveau; i++) //Placer le curseur au bon endroit
        fgets(lignefichier,TAILLLE_MAX,fichier);

        putc('\n',temp);
     while (fgets(lignefichier, TAILLLE_MAX, fichier) != NULL)
     {
         fprintf(temp,"%s",lignefichier);
     }

    fclose(fichier);
    fclose(temp);
}

void copiercoller()
{
    FILE *temp = NULL, *fichier=NULL;
    int lignefichier[TAILLLE_MAX];

    fichier = fopen("test.lvl","w+"); // ETAPE 4
    temp = fopen("temp.lvl","r");

    while (fgets(lignefichier, TAILLLE_MAX, temp) != NULL)
     {
         fprintf(fichier,"%s",lignefichier);

     }
    fclose(fichier);
    fclose(temp);
}
